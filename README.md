LCD/SPI Adapter for RAMPS-FD
============================

This is a fork of
[https://github.com/bobc/bobc_hardware/tree/master/LCD-panel-adapter-FD](https://github.com/bobc/bobc_hardware/tree/master/LCD-panel-adapter-FD), in
which I extracted the LCD-adapter subtree from the repository, brought it
into KiCad 5, and added SPI headers to control TMC2130s.

The bobc branch is the original, without SPI and using a 74HC07 instead of
the current 74LCX07.  The 74hct244-didnt-work branch is where I tried
subbing a 74HCT244...made one, found that the SD-card slot didn't work
reliably.  Hopefully the 74LCX07 proves to be a suitable replacement, as
74HC07s only appear to be available from eBay sellers and such at this time.
