EESchema Schematic File Version 4
LIBS:LCD-panel-adapter-lvc-cache
EELAYER 26 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 1 1
Title ""
Date "2018-08-14"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L LCD-panel-adapter-lvc-rescue:CONN_5X2 P102
U 1 1 52434ED1
P 8300 3600
F 0 "P102" H 8300 3900 60  0000 C CNN
F 1 "EXP1" V 8300 3600 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x05_P2.54mm_Vertical" H 8300 3600 60  0001 C CNN
F 3 "https://s3.amazonaws.com/catalogspreads-pdf/PAGE110-111%20.100%20MALE%20HDR%20ST.pdf" H 8300 3600 60  0001 C CNN
F 4 "" H 0   0   50  0001 C CNN "Critical"
F 5 "Connector Header Through Hole 10 position 0.100\" (2.54mm)" H 0   0   50  0001 C CNN "Description"
F 6 "Sullins" H 0   0   50  0001 C CNN "DK_Mfr"
F 7 "S2011EC-05-ND" H 0   0   50  0001 C CNN "DK_PN"
F 8 "y" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "PRPC005DAAN-RC" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 10 "Shenzhen Cankemeng" H 0   0   50  0001 C CNN "LCSC_Mfr"
F 11 "C124387" H 0   0   50  0001 C CNN "LCSC_PN"
	1    8300 3600
	1    0    0    -1  
$EndComp
$Comp
L LCD-panel-adapter-lvc-rescue:CONN_5X2 P103
U 1 1 52434ED7
P 8350 4650
F 0 "P103" H 8350 4950 60  0000 C CNN
F 1 "EXP2" V 8350 4650 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x05_P2.54mm_Vertical" H 8350 4650 60  0001 C CNN
F 3 "https://s3.amazonaws.com/catalogspreads-pdf/PAGE110-111%20.100%20MALE%20HDR%20ST.pdf" H 8350 4650 60  0001 C CNN
F 4 "" H 0   0   50  0001 C CNN "Critical"
F 5 "Connector Header Through Hole 10 position 0.100\" (2.54mm)" H 0   0   50  0001 C CNN "Description"
F 6 "Sullins" H 0   0   50  0001 C CNN "DK_Mfr"
F 7 "S2011EC-05-ND" H 0   0   50  0001 C CNN "DK_PN"
F 8 "y" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "PRPC005DAAN-RC" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 10 "Shenzhen Cankemeng" H 0   0   50  0001 C CNN "LCSC_Mfr"
F 11 "C124387" H 0   0   50  0001 C CNN "LCSC_PN"
	1    8350 4650
	1    0    0    -1  
$EndComp
$Comp
L LCD-panel-adapter-lvc-rescue:GND #PWR01
U 1 1 52434EDD
P 7400 3950
F 0 "#PWR01" H 7400 3950 30  0001 C CNN
F 1 "GND" H 7400 3880 30  0001 C CNN
F 2 "" H 7400 3950 60  0000 C CNN
F 3 "" H 7400 3950 60  0000 C CNN
	1    7400 3950
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR02
U 1 1 52434EE4
P 9350 3800
F 0 "#PWR02" H 9350 3890 20  0001 C CNN
F 1 "+5V" H 9350 3890 30  0000 C CNN
F 2 "" H 9350 3800 60  0000 C CNN
F 3 "" H 9350 3800 60  0000 C CNN
	1    9350 3800
	1    0    0    -1  
$EndComp
NoConn ~ 8750 4850
NoConn ~ 7950 4850
Wire Wire Line
	9350 3800 8700 3800
Wire Wire Line
	8800 3700 8700 3700
Wire Wire Line
	7900 3700 7400 3700
Wire Wire Line
	8800 3600 8700 3600
Wire Wire Line
	7900 3600 7400 3600
Wire Wire Line
	7900 3800 7400 3800
Wire Wire Line
	7400 3800 7400 3950
Wire Wire Line
	7900 3400 7400 3400
Wire Wire Line
	7400 3500 7900 3500
Wire Wire Line
	8800 3500 8700 3500
Wire Wire Line
	8700 3400 8800 3400
Wire Wire Line
	7550 4750 7950 4750
Wire Wire Line
	7950 4650 7550 4650
Wire Wire Line
	7550 4550 7950 4550
Wire Wire Line
	7950 4450 7550 4450
Wire Wire Line
	8750 4450 8900 4450
Wire Wire Line
	8750 4550 8900 4550
Wire Wire Line
	8750 4650 8900 4650
Wire Wire Line
	8900 4750 8750 4750
$Comp
L LCD-panel-adapter-lvc-rescue:HEADER_18 J101
U 1 1 52434F14
P 1650 3850
F 0 "J101" H 1650 4800 60  0000 C CNN
F 1 "AUX4" H 1650 2900 60  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x18_P2.54mm_Vertical" H 1650 3850 60  0001 C CNN
F 3 "https://drawings-pdf.s3.amazonaws.com/10492.pdf" H 1650 3850 60  0001 C CNN
F 4 "" H 0   0   50  0001 C CNN "Critical"
F 5 "18 Position Header Connector 0.100\" (2.54mm) Through Hole Tin" H 0   0   50  0001 C CNN "Description"
F 6 "Sullins" H 0   0   50  0001 C CNN "DK_Mfr"
F 7 "S7016-ND" H 0   0   50  0001 C CNN "DK_PN"
F 8 "y" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "PPTC181LFBN-RC" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 10 "Boom Precision Elec" H 0   0   50  0001 C CNN "LCSC_Mfr"
F 11 "C50984" H 0   0   50  0001 C CNN "LCSC_PN"
F 12 "LCSC part is a 20-pin device; trim to fit" H 0   0   50  0001 C CNN "Notes"
	1    1650 3850
	-1   0    0    -1  
$EndComp
$Comp
L LCD-panel-adapter-lvc-rescue:GND #PWR03
U 1 1 52434F1A
P 1250 1750
F 0 "#PWR03" H 1250 1750 30  0001 C CNN
F 1 "GND" H 1250 1680 30  0001 C CNN
F 2 "" H 1250 1750 60  0000 C CNN
F 3 "" H 1250 1750 60  0000 C CNN
	1    1250 1750
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1850 1600 1250 1600
$Comp
L power:+5V #PWR04
U 1 1 52434F23
P 1250 1150
F 0 "#PWR04" H 1250 1240 20  0001 C CNN
F 1 "+5V" H 1250 1240 30  0000 C CNN
F 2 "" H 1250 1150 60  0000 C CNN
F 3 "" H 1250 1150 60  0000 C CNN
	1    1250 1150
	-1   0    0    -1  
$EndComp
NoConn ~ 2650 1600
Wire Wire Line
	1250 1300 1850 1300
Wire Wire Line
	1850 1500 1500 1500
Wire Wire Line
	1500 1400 1850 1400
Wire Wire Line
	2650 1500 2800 1500
Wire Wire Line
	2800 1400 2650 1400
Wire Wire Line
	2650 1300 2800 1300
NoConn ~ 1750 3200
NoConn ~ 1750 3300
NoConn ~ 1750 3400
NoConn ~ 1750 3500
NoConn ~ 1750 3700
Wire Wire Line
	1850 4100 1750 4100
Wire Wire Line
	1750 4000 1850 4000
Wire Wire Line
	1850 3900 1750 3900
Wire Wire Line
	1750 3800 1850 3800
Wire Wire Line
	1850 3600 1750 3600
Wire Wire Line
	1250 1150 1250 1300
Wire Wire Line
	1250 1600 1250 1750
Text Label 6100 4200 0    50   ~ 0
LCD_D7_5V
Text Label 6100 4300 0    50   ~ 0
LCD_D6_5V
Text Label 6100 4400 0    50   ~ 0
LCD_D5_5V
Text Label 6100 4500 0    50   ~ 0
LCD_D4_5V
Text Label 6100 4600 0    50   ~ 0
LCD_E_5V
Text Label 6100 4700 0    50   ~ 0
LCD_RS_5V
Text Notes 1650 2700 0    60   ~ 0
IOREF can be 3.3V or 5V
Text Notes 3000 1100 0    60   ~ 0
These signals can be 3V/5V
Text Label 1950 3000 0    60   ~ 0
IOREF
Wire Wire Line
	1750 3000 2400 3000
Text Notes 1550 4950 0    60   ~ 0
These signals can be 3V/5V
$Comp
L power:+5V #PWR06
U 1 1 524354D6
P 2300 7000
F 0 "#PWR06" H 2300 7090 20  0001 C CNN
F 1 "+5V" H 2300 7090 30  0000 C CNN
F 2 "" H 2300 7000 60  0000 C CNN
F 3 "" H 2300 7000 60  0000 C CNN
	1    2300 7000
	-1   0    0    -1  
$EndComp
$Comp
L LCD-panel-adapter-lvc-rescue:GND #PWR07
U 1 1 524354DC
P 2650 7250
F 0 "#PWR07" H 2650 7250 30  0001 C CNN
F 1 "GND" H 2650 7180 30  0001 C CNN
F 2 "" H 2650 7250 60  0000 C CNN
F 3 "" H 2650 7250 60  0000 C CNN
	1    2650 7250
	-1   0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG08
U 1 1 524354F1
P 2650 6950
F 0 "#FLG08" H 2650 7045 30  0001 C CNN
F 1 "PWR_FLAG" H 2650 7130 30  0000 C CNN
F 2 "" H 2650 6950 60  0000 C CNN
F 3 "" H 2650 6950 60  0000 C CNN
	1    2650 6950
	1    0    0    -1  
$EndComp
Wire Wire Line
	2650 6950 2650 7250
$Comp
L power:PWR_FLAG #FLG09
U 1 1 5243553E
P 2300 7250
F 0 "#FLG09" H 2300 7345 30  0001 C CNN
F 1 "PWR_FLAG" H 2300 7430 30  0000 C CNN
F 2 "" H 2300 7250 60  0000 C CNN
F 3 "" H 2300 7250 60  0000 C CNN
	1    2300 7250
	-1   0    0    1   
$EndComp
Wire Wire Line
	2300 7250 2300 7000
$Comp
L power:PWR_FLAG #FLG010
U 1 1 52435588
P 2400 2900
F 0 "#FLG010" H 2400 2995 30  0001 C CNN
F 1 "PWR_FLAG" H 2400 3080 30  0000 C CNN
F 2 "" H 2400 2900 60  0000 C CNN
F 3 "" H 2400 2900 60  0000 C CNN
	1    2400 2900
	1    0    0    -1  
$EndComp
Text Label 8800 3500 0    50   ~ 0
LCD_RS_5V
Text Label 7400 3500 0    50   ~ 0
LCD_E_5V
Text Label 7400 3600 0    50   ~ 0
LCD_D4_5V
Text Label 7400 3700 0    50   ~ 0
LCD_D6_5V
Text Label 8800 3600 0    50   ~ 0
LCD_D5_5V
Text Label 8800 3700 0    50   ~ 0
LCD_D7_5V
Wire Wire Line
	2400 3000 2400 2900
Wire Wire Line
	3300 5450 3900 5450
$Comp
L LCD-panel-adapter-lvc-rescue:C C102
U 1 1 524366E1
P 4650 6900
F 0 "C102" H 4650 7000 40  0000 L CNN
F 1 "100nF" H 4656 6815 40  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4688 6750 30  0001 C CNN
F 3 "https://api.kemet.com/component-edge/download/datasheet/C0805C104Z5VACTU.pdf" H 4650 6900 60  0001 C CNN
F 4 "" H 0   0   50  0001 C CNN "Critical"
F 5 "0.1µF -20%, +80% 50V Ceramic Capacitor Y5V (F) 0805 (2012 Metric)" H 0   0   50  0001 C CNN "Description"
F 6 "Kemet" H 0   0   50  0001 C CNN "DK_Mfr"
F 7 "399-1177-1-ND" H 0   0   50  0001 C CNN "DK_PN"
F 8 "0805" H 0   0   50  0001 C CNN "Package"
F 9 "y" H 0   0   50  0001 C CNN "Src Any/Spec"
F 10 "C0805C104Z5VACTU" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 11 "Yageo" H 0   0   50  0001 C CNN "LCSC_Mfr"
F 12 "CC0805ZRY5V9BB104" H 0   0   50  0001 C CNN "LCSC_Mfr_PN"
F 13 "C111496" H 0   0   50  0001 C CNN "LCSC_PN"
	1    4650 6900
	1    0    0    -1  
$EndComp
$Comp
L LCD-panel-adapter-lvc-rescue:GND #PWR011
U 1 1 524366E7
P 4650 7250
F 0 "#PWR011" H 4650 7250 30  0001 C CNN
F 1 "GND" H 4650 7180 30  0001 C CNN
F 2 "" H 4650 7250 60  0000 C CNN
F 3 "" H 4650 7250 60  0000 C CNN
	1    4650 7250
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4650 6550 4650 6700
Text Label 4650 6550 0    60   ~ 0
IOREF
$Comp
L LCD-panel-adapter-lvc-rescue:GND #PWR012
U 1 1 524369ED
P 2400 3150
F 0 "#PWR012" H 2400 3150 30  0001 C CNN
F 1 "GND" H 2400 3080 30  0001 C CNN
F 2 "" H 2400 3150 60  0000 C CNN
F 3 "" H 2400 3150 60  0000 C CNN
	1    2400 3150
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2400 3150 2400 3100
Wire Wire Line
	2400 3100 1750 3100
Text Label 3300 5450 0    60   ~ 0
IOREF
Connection ~ 5000 4200
Connection ~ 5150 4300
Connection ~ 5300 4400
Connection ~ 5450 4500
Connection ~ 5600 4600
Connection ~ 5750 4700
$Comp
L power:+5V #PWR013
U 1 1 5245E3A5
P 5000 3200
F 0 "#PWR013" H 5000 3290 20  0001 C CNN
F 1 "+5V" H 5000 3290 30  0000 C CNN
F 2 "" H 5000 3200 60  0000 C CNN
F 3 "" H 5000 3200 60  0000 C CNN
	1    5000 3200
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5000 3200 5000 3400
Wire Wire Line
	5000 3400 5150 3400
Connection ~ 5000 3400
$Comp
L Device:R_US R101
U 1 1 52460B2A
P 5000 3750
F 0 "R101" V 4950 3900 40  0000 C CNN
F 1 "10k" V 5050 3600 40  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4930 3750 30  0001 C CNN
F 3 "http://industrial.panasonic.com/www-cgi/jvcr13pz.cgi?E+PZ+3+AOA0001+ERJ6GEYJ103V+7+WW" H 5000 3750 30  0001 C CNN
F 4 "" H 0   0   50  0001 C CNN "Critical"
F 5 "10 kOhms ±5% 0.125W, 1/8W Chip Resistor 0805 (2012 Metric) Automotive AEC-Q200 Thick Film" H 0   0   50  0001 C CNN "Description"
F 6 "Panasonic" H 0   0   50  0001 C CNN "DK_Mfr"
F 7 "P10KACT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 8 "0805" H 0   0   50  0001 C CNN "Package"
F 9 "y" H 0   0   50  0001 C CNN "Src Any/Spec"
F 10 "ERJ-6GEYJ103V" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 11 "Yageo" H 0   0   50  0001 C CNN "LCSC_Mfr"
F 12 "RC0805JR-0710KL" H 0   0   50  0001 C CNN "LCSC_Mfr_PN"
F 13 "C100047" H 0   0   50  0001 C CNN "LCSC_PN"
	1    5000 3750
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R102
U 1 1 52460B3C
P 5150 3750
F 0 "R102" V 5100 3900 40  0000 C CNN
F 1 "10k" V 5200 3600 40  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5080 3750 30  0001 C CNN
F 3 "http://industrial.panasonic.com/www-cgi/jvcr13pz.cgi?E+PZ+3+AOA0001+ERJ6GEYJ103V+7+WW" H 5150 3750 30  0001 C CNN
F 4 "" H 0   0   50  0001 C CNN "Critical"
F 5 "10 kOhms ±5% 0.125W, 1/8W Chip Resistor 0805 (2012 Metric) Automotive AEC-Q200 Thick Film" H 0   0   50  0001 C CNN "Description"
F 6 "Panasonic" H 0   0   50  0001 C CNN "DK_Mfr"
F 7 "P10KACT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 8 "0805" H 0   0   50  0001 C CNN "Package"
F 9 "y" H 0   0   50  0001 C CNN "Src Any/Spec"
F 10 "ERJ-6GEYJ103V" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 11 "Yageo" H 0   0   50  0001 C CNN "LCSC_Mfr"
F 12 "RC0805JR-0710KL" H 0   0   50  0001 C CNN "LCSC_Mfr_PN"
F 13 "C100047" H 0   0   50  0001 C CNN "LCSC_PN"
	1    5150 3750
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R103
U 1 1 52460B42
P 5300 3750
F 0 "R103" V 5250 3900 40  0000 C CNN
F 1 "10k" V 5350 3600 40  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5230 3750 30  0001 C CNN
F 3 "http://industrial.panasonic.com/www-cgi/jvcr13pz.cgi?E+PZ+3+AOA0001+ERJ6GEYJ103V+7+WW" H 5300 3750 30  0001 C CNN
F 4 "" H 0   0   50  0001 C CNN "Critical"
F 5 "10 kOhms ±5% 0.125W, 1/8W Chip Resistor 0805 (2012 Metric) Automotive AEC-Q200 Thick Film" H 0   0   50  0001 C CNN "Description"
F 6 "Panasonic" H 0   0   50  0001 C CNN "DK_Mfr"
F 7 "P10KACT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 8 "0805" H 0   0   50  0001 C CNN "Package"
F 9 "y" H 0   0   50  0001 C CNN "Src Any/Spec"
F 10 "ERJ-6GEYJ103V" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 11 "Yageo" H 0   0   50  0001 C CNN "LCSC_Mfr"
F 12 "RC0805JR-0710KL" H 0   0   50  0001 C CNN "LCSC_Mfr_PN"
F 13 "C100047" H 0   0   50  0001 C CNN "LCSC_PN"
	1    5300 3750
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R104
U 1 1 52460B48
P 5450 3750
F 0 "R104" V 5400 3900 40  0000 C CNN
F 1 "10k" V 5500 3600 40  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5380 3750 30  0001 C CNN
F 3 "http://industrial.panasonic.com/www-cgi/jvcr13pz.cgi?E+PZ+3+AOA0001+ERJ6GEYJ103V+7+WW" H 5450 3750 30  0001 C CNN
F 4 "" H 0   0   50  0001 C CNN "Critical"
F 5 "10 kOhms ±5% 0.125W, 1/8W Chip Resistor 0805 (2012 Metric) Automotive AEC-Q200 Thick Film" H 0   0   50  0001 C CNN "Description"
F 6 "Panasonic" H 0   0   50  0001 C CNN "DK_Mfr"
F 7 "P10KACT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 8 "0805" H 0   0   50  0001 C CNN "Package"
F 9 "y" H 0   0   50  0001 C CNN "Src Any/Spec"
F 10 "ERJ-6GEYJ103V" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 11 "Yageo" H 0   0   50  0001 C CNN "LCSC_Mfr"
F 12 "RC0805JR-0710KL" H 0   0   50  0001 C CNN "LCSC_Mfr_PN"
F 13 "C100047" H 0   0   50  0001 C CNN "LCSC_PN"
	1    5450 3750
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R105
U 1 1 52460B4E
P 5600 3750
F 0 "R105" V 5550 3900 40  0000 C CNN
F 1 "10k" V 5650 3600 40  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5530 3750 30  0001 C CNN
F 3 "http://industrial.panasonic.com/www-cgi/jvcr13pz.cgi?E+PZ+3+AOA0001+ERJ6GEYJ103V+7+WW" H 5600 3750 30  0001 C CNN
F 4 "" H 0   0   50  0001 C CNN "Critical"
F 5 "10 kOhms ±5% 0.125W, 1/8W Chip Resistor 0805 (2012 Metric) Automotive AEC-Q200 Thick Film" H 0   0   50  0001 C CNN "Description"
F 6 "Panasonic" H 0   0   50  0001 C CNN "DK_Mfr"
F 7 "P10KACT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 8 "0805" H 0   0   50  0001 C CNN "Package"
F 9 "y" H 0   0   50  0001 C CNN "Src Any/Spec"
F 10 "ERJ-6GEYJ103V" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 11 "Yageo" H 0   0   50  0001 C CNN "LCSC_Mfr"
F 12 "RC0805JR-0710KL" H 0   0   50  0001 C CNN "LCSC_Mfr_PN"
F 13 "C100047" H 0   0   50  0001 C CNN "LCSC_PN"
	1    5600 3750
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R106
U 1 1 52460B54
P 5750 3750
F 0 "R106" V 5700 3900 40  0000 C CNN
F 1 "10k" V 5800 3600 40  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5680 3750 30  0001 C CNN
F 3 "http://industrial.panasonic.com/www-cgi/jvcr13pz.cgi?E+PZ+3+AOA0001+ERJ6GEYJ103V+7+WW" H 5750 3750 30  0001 C CNN
F 4 "" H 0   0   50  0001 C CNN "Critical"
F 5 "10 kOhms ±5% 0.125W, 1/8W Chip Resistor 0805 (2012 Metric) Automotive AEC-Q200 Thick Film" H 0   0   50  0001 C CNN "Description"
F 6 "Panasonic" H 0   0   50  0001 C CNN "DK_Mfr"
F 7 "P10KACT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 8 "0805" H 0   0   50  0001 C CNN "Package"
F 9 "y" H 0   0   50  0001 C CNN "Src Any/Spec"
F 10 "ERJ-6GEYJ103V" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 11 "Yageo" H 0   0   50  0001 C CNN "LCSC_Mfr"
F 12 "RC0805JR-0710KL" H 0   0   50  0001 C CNN "LCSC_Mfr_PN"
F 13 "C100047" H 0   0   50  0001 C CNN "LCSC_PN"
	1    5750 3750
	1    0    0    -1  
$EndComp
Connection ~ 5600 3400
Connection ~ 5450 3400
Connection ~ 5300 3400
Connection ~ 5150 3400
$Comp
L LCD-panel-adapter-lvc-rescue:LOGO_OSHW M101
U 1 1 52461DD4
P 10050 6350
F 0 "M101" H 10050 6500 60  0000 C CNN
F 1 "LOGO_OSHW" H 10050 6250 60  0000 C CNN
F 2 "OSHW_logo_2" H 9950 6400 60  0001 C CNN
F 3 "" H 10050 6350 60  0000 C CNN
	1    10050 6350
	1    0    0    -1  
$EndComp
Text GLabel 1850 3600 2    50   Input ~ 0
RESET
Text GLabel 1850 4200 2    50   Output ~ 0
LCD_D7
Text GLabel 1850 4300 2    50   Output ~ 0
LCD_D6
Text GLabel 1850 4400 2    50   Output ~ 0
LCD_D5
Text GLabel 1850 4500 2    50   Output ~ 0
LCD_D4
Text GLabel 1850 4600 2    50   Output ~ 0
LCD_E
Text GLabel 1850 4700 2    50   Output ~ 0
LCD_RS
Text GLabel 2800 1400 2    50   Input ~ 0
SD_DI
Text GLabel 2800 1500 2    50   Output ~ 0
SD_CS
Text GLabel 1500 1400 0    50   Output ~ 0
SD_DO
Text GLabel 1500 1500 0    50   Output ~ 0
SD_CLK
Text GLabel 2800 1300 2    50   Input ~ 0
SD_DETECT
Text GLabel 1850 3800 2    50   Output ~ 0
BEEP
Text GLabel 1850 3900 2    50   Input ~ 0
ENC_SW
Text GLabel 1850 4000 2    50   Input ~ 0
ENC1
Text GLabel 1850 4100 2    50   Input ~ 0
ENC2
Text GLabel 7400 3400 0    50   Input ~ 0
BEEP
Text GLabel 8800 3400 2    50   Output ~ 0
ENC_SW
Text GLabel 7550 4450 0    50   Input ~ 0
SD_DO
Text GLabel 7550 4550 0    50   Output ~ 0
ENC2
Text GLabel 7550 4650 0    50   Output ~ 0
ENC1
Text GLabel 7550 4750 0    50   Output ~ 0
SD_DETECT
Text GLabel 8900 4450 2    50   Input ~ 0
SD_CLK
Text GLabel 8900 4550 2    50   Input ~ 0
SD_CS
Text GLabel 8900 4650 2    50   Output ~ 0
SD_DI
Text GLabel 8900 4750 2    50   Output ~ 0
RESET
$Comp
L LCD-panel-adapter-lvc-rescue:CONN_6X2 P101
U 1 1 5251E207
P 2250 1550
F 0 "P101" H 2250 1900 60  0000 C CNN
F 1 "AUX3" V 2250 1550 60  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x06_P2.54mm_Vertical" H 2250 1550 60  0001 C CNN
F 3 "https://drawings-pdf.s3.amazonaws.com/10492.pdf" H 2250 1550 60  0001 C CNN
F 4 "" H 0   0   50  0001 C CNN "Critical"
F 5 "12 Position Header Connector 0.100\" (2.54mm) Through Hole Tin" H 0   0   50  0001 C CNN "Description"
F 6 "Sullins" H 0   0   50  0001 C CNN "DK_Mfr"
F 7 "S7074-ND" H 0   0   50  0001 C CNN "DK_PN"
F 8 "y" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "PPTC062LFBN-RC" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 10 "MINTRON" H 0   0   50  0001 C CNN "LCSC_Mfr"
F 11 "MTF185-206SY1" H 0   0   50  0001 C CNN "LCSC_Mfr_PN"
F 12 "C358728" H 0   0   50  0001 C CNN "LCSC_PN"
	1    2250 1550
	1    0    0    -1  
$EndComp
NoConn ~ 2650 1700
NoConn ~ 2650 1800
NoConn ~ 1850 1800
NoConn ~ 1850 1700
Wire Notes Line
	1200 900  1000 900 
Wire Notes Line
	1000 900  1000 5000
Wire Notes Line
	1000 5000 1250 5000
Text Notes 550  2450 0    50   ~ 0
RAMPS-FD
Text Notes 10000 4050 0    50   ~ 0
LCD Panel
Wire Notes Line
	9600 3100 9700 3100
Wire Notes Line
	9700 3100 9700 5050
Wire Notes Line
	9700 5050 9600 5050
Wire Notes Line
	9950 4000 9700 4000
Wire Wire Line
	5000 4200 6100 4200
Wire Wire Line
	5150 4300 6100 4300
Wire Wire Line
	5300 4400 6100 4400
Wire Wire Line
	5450 4500 6100 4500
Wire Wire Line
	5600 4600 6100 4600
Wire Wire Line
	5750 4700 6100 4700
Wire Wire Line
	5600 3400 5750 3400
Wire Wire Line
	5450 3400 5600 3400
Wire Wire Line
	5300 3400 5450 3400
Wire Wire Line
	5150 3400 5300 3400
Wire Wire Line
	4650 7100 4650 7250
Wire Wire Line
	5000 3400 5000 3600
Wire Wire Line
	5000 3900 5000 4200
Wire Wire Line
	5150 3400 5150 3600
Wire Wire Line
	5150 3900 5150 4300
Wire Wire Line
	5300 3400 5300 3600
Wire Wire Line
	5300 3900 5300 4400
Wire Wire Line
	5450 3400 5450 3600
Wire Wire Line
	5450 3900 5450 4500
Wire Wire Line
	5600 3400 5600 3600
Wire Wire Line
	5600 3900 5600 4600
Wire Wire Line
	5750 3400 5750 3600
Wire Wire Line
	5750 3900 5750 4700
Text Notes 3400 3000 0    50   ~ 0
Replace 74HCT244 with 74LCX07 buffer as current replacement for 74HC07...\nneed open-drain outputs?
$Comp
L 74xx:74LCX07 U1
U 1 1 5C7FBDA9
P 3550 3850
F 0 "U1" H 3550 4000 50  0000 C CNN
F 1 "74LCX07" H 3550 4076 50  0001 C CNN
F 2 "Package_SO:TSSOP-14_4.4x5mm_P0.65mm" H 3550 3850 50  0001 C CNN
F 3 "https://www2.mouser.com/datasheet/2/308/74LCX07-1299942.pdf" H 3550 3850 50  0001 C CNN
F 4 "ON Semiconductor" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "MC74LCX07DTR2G" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "MC74LCX07DTR2GOSCT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "TSSOP-14" H 0   0   50  0001 C CNN "Package"
F 8 "y" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "Buffer, Non-Inverting 6 Element 1 Bit per Element Open Drain Output 14-TSSOP" H 0   0   50  0001 C CNN "Description"
F 10 "ON Semiconductor" H 0   0   50  0001 C CNN "LCSC_Mfr"
F 11 "74LCX07MTCX" H 0   0   50  0001 C CNN "LCSC_Mfr_PN"
F 12 "C6006" H 0   0   50  0001 C CNN "LCSC_PN"
	1    3550 3850
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LCX07 U1
U 2 1 5C802561
P 4100 4050
F 0 "U1" H 4100 4200 50  0000 C CNN
F 1 "74LCX07" H 4100 4276 50  0001 C CNN
F 2 "Package_SO:TSSOP-14_4.4x5mm_P0.65mm" H 4100 4050 50  0001 C CNN
F 3 "https://www2.mouser.com/datasheet/2/308/74LCX07-1299942.pdf" H 4100 4050 50  0001 C CNN
F 4 "ON Semiconductor" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "MC74LCX07DTR2G" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "MC74LCX07DTR2GOSCT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "TSSOP-14" H 0   0   50  0001 C CNN "Package"
F 8 "y" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "Buffer, Non-Inverting 6 Element 1 Bit per Element Open Drain Output 14-TSSOP" H 0   0   50  0001 C CNN "Description"
F 10 "ON Semiconductor" H 0   0   50  0001 C CNN "LCSC_Mfr"
F 11 "74LCX07MTCX" H 0   0   50  0001 C CNN "LCSC_Mfr_PN"
F 12 "C6006" H 0   0   50  0001 C CNN "LCSC_PN"
	2    4100 4050
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LCX07 U1
U 3 1 5C8025D6
P 3550 4350
F 0 "U1" H 3550 4500 50  0000 C CNN
F 1 "74LCX07" H 3550 4576 50  0001 C CNN
F 2 "Package_SO:TSSOP-14_4.4x5mm_P0.65mm" H 3550 4350 50  0001 C CNN
F 3 "https://www2.mouser.com/datasheet/2/308/74LCX07-1299942.pdf" H 3550 4350 50  0001 C CNN
F 4 "ON Semiconductor" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "MC74LCX07DTR2G" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "MC74LCX07DTR2GOSCT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "TSSOP-14" H 0   0   50  0001 C CNN "Package"
F 8 "y" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "Buffer, Non-Inverting 6 Element 1 Bit per Element Open Drain Output 14-TSSOP" H 0   0   50  0001 C CNN "Description"
F 10 "ON Semiconductor" H 0   0   50  0001 C CNN "LCSC_Mfr"
F 11 "74LCX07MTCX" H 0   0   50  0001 C CNN "LCSC_Mfr_PN"
F 12 "C6006" H 0   0   50  0001 C CNN "LCSC_PN"
	3    3550 4350
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LCX07 U1
U 4 1 5C802650
P 4100 4550
F 0 "U1" H 4100 4700 50  0000 C CNN
F 1 "74LCX07" H 4100 4776 50  0001 C CNN
F 2 "Package_SO:TSSOP-14_4.4x5mm_P0.65mm" H 4100 4550 50  0001 C CNN
F 3 "https://www2.mouser.com/datasheet/2/308/74LCX07-1299942.pdf" H 4100 4550 50  0001 C CNN
F 4 "ON Semiconductor" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "MC74LCX07DTR2G" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "MC74LCX07DTR2GOSCT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "TSSOP-14" H 0   0   50  0001 C CNN "Package"
F 8 "y" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "Buffer, Non-Inverting 6 Element 1 Bit per Element Open Drain Output 14-TSSOP" H 0   0   50  0001 C CNN "Description"
F 10 "ON Semiconductor" H 0   0   50  0001 C CNN "LCSC_Mfr"
F 11 "74LCX07MTCX" H 0   0   50  0001 C CNN "LCSC_Mfr_PN"
F 12 "C6006" H 0   0   50  0001 C CNN "LCSC_PN"
	4    4100 4550
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LCX07 U1
U 5 1 5C8026CB
P 3550 4800
F 0 "U1" H 3550 4950 50  0000 C CNN
F 1 "74LCX07" H 3550 5026 50  0001 C CNN
F 2 "Package_SO:TSSOP-14_4.4x5mm_P0.65mm" H 3550 4800 50  0001 C CNN
F 3 "https://www2.mouser.com/datasheet/2/308/74LCX07-1299942.pdf" H 3550 4800 50  0001 C CNN
F 4 "ON Semiconductor" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "MC74LCX07DTR2G" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "MC74LCX07DTR2GOSCT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "TSSOP-14" H 0   0   50  0001 C CNN "Package"
F 8 "y" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "Buffer, Non-Inverting 6 Element 1 Bit per Element Open Drain Output 14-TSSOP" H 0   0   50  0001 C CNN "Description"
F 10 "ON Semiconductor" H 0   0   50  0001 C CNN "LCSC_Mfr"
F 11 "74LCX07MTCX" H 0   0   50  0001 C CNN "LCSC_Mfr_PN"
F 12 "C6006" H 0   0   50  0001 C CNN "LCSC_PN"
	5    3550 4800
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LCX07 U1
U 6 1 5C80274B
P 4050 5050
F 0 "U1" H 4050 5200 50  0000 C CNN
F 1 "74LCX07" H 4050 5276 50  0001 C CNN
F 2 "Package_SO:TSSOP-14_4.4x5mm_P0.65mm" H 4050 5050 50  0001 C CNN
F 3 "https://www2.mouser.com/datasheet/2/308/74LCX07-1299942.pdf" H 4050 5050 50  0001 C CNN
F 4 "ON Semiconductor" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "MC74LCX07DTR2G" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "MC74LCX07DTR2GOSCT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "TSSOP-14" H 0   0   50  0001 C CNN "Package"
F 8 "y" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "Buffer, Non-Inverting 6 Element 1 Bit per Element Open Drain Output 14-TSSOP" H 0   0   50  0001 C CNN "Description"
F 10 "ON Semiconductor" H 0   0   50  0001 C CNN "LCSC_Mfr"
F 11 "74LCX07MTCX" H 0   0   50  0001 C CNN "LCSC_Mfr_PN"
F 12 "C6006" H 0   0   50  0001 C CNN "LCSC_PN"
	6    4050 5050
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LCX07 U1
U 7 1 5C8027CE
P 3900 5950
F 0 "U1" H 4130 5996 50  0000 L CNN
F 1 "74LCX07" H 4130 5905 50  0000 L CNN
F 2 "Package_SO:TSSOP-14_4.4x5mm_P0.65mm" H 3900 5950 50  0001 C CNN
F 3 "https://www2.mouser.com/datasheet/2/308/74LCX07-1299942.pdf" H 3900 5950 50  0001 C CNN
F 4 "ON Semiconductor" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "MC74LCX07DTR2G" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "MC74LCX07DTR2GOSCT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "TSSOP-14" H 0   0   50  0001 C CNN "Package"
F 8 "y" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "Buffer, Non-Inverting 6 Element 1 Bit per Element Open Drain Output 14-TSSOP" H 0   0   50  0001 C CNN "Description"
F 10 "ON Semiconductor" H 0   0   50  0001 C CNN "LCSC_Mfr"
F 11 "74LCX07MTCX" H 0   0   50  0001 C CNN "LCSC_Mfr_PN"
F 12 "C6006" H 0   0   50  0001 C CNN "LCSC_PN"
	7    3900 5950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3850 4350 4600 4350
Wire Wire Line
	4600 4350 4600 4400
Wire Wire Line
	4600 4400 5300 4400
Wire Wire Line
	4400 4550 4600 4550
Wire Wire Line
	4600 4550 4600 4500
Wire Wire Line
	4600 4500 5450 4500
Wire Wire Line
	4400 4050 4650 4050
Wire Wire Line
	4650 4050 4650 4300
Wire Wire Line
	4650 4300 5150 4300
Wire Wire Line
	3850 3850 4700 3850
Wire Wire Line
	4700 3850 4700 4200
Wire Wire Line
	4700 4200 5000 4200
Wire Wire Line
	3850 4800 4650 4800
Wire Wire Line
	4650 4800 4650 4600
Wire Wire Line
	4650 4600 5600 4600
Wire Wire Line
	4350 5050 4700 5050
Wire Wire Line
	4700 5050 4700 4700
Wire Wire Line
	4700 4700 5750 4700
Wire Wire Line
	3100 4500 3100 4550
Wire Wire Line
	3100 4550 3800 4550
Wire Wire Line
	1750 4500 3100 4500
Wire Wire Line
	3100 4400 3100 4350
Wire Wire Line
	3100 4350 3250 4350
Wire Wire Line
	1750 4400 3100 4400
Wire Wire Line
	3050 4300 3050 4050
Wire Wire Line
	3050 4050 3800 4050
Wire Wire Line
	1750 4300 3050 4300
Wire Wire Line
	3000 4200 3000 3850
Wire Wire Line
	3000 3850 3250 3850
Wire Wire Line
	1750 4200 3000 4200
Wire Wire Line
	3050 4600 3050 4800
Wire Wire Line
	3050 4800 3250 4800
Wire Wire Line
	1750 4600 3050 4600
Wire Wire Line
	3000 4700 3000 5050
Wire Wire Line
	3000 5050 3750 5050
Wire Wire Line
	1750 4700 3000 4700
$Comp
L LCD-panel-adapter-lvc-rescue:GND #PWR08
U 1 1 5C8367DB
P 3900 6500
F 0 "#PWR08" H 3900 6500 30  0001 C CNN
F 1 "GND" H 3900 6430 30  0001 C CNN
F 2 "" H 3900 6500 60  0000 C CNN
F 3 "" H 3900 6500 60  0000 C CNN
	1    3900 6500
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3900 6500 3900 6450
$Comp
L Connector_Generic:Conn_01x08 J2
U 1 1 5C83DC17
P 6550 1550
F 0 "J2" H 6500 1950 50  0000 L CNN
F 1 "Conn_01x08" H 6630 1451 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x08_P2.54mm_Vertical" H 6550 1550 50  0001 C CNN
F 3 "https://media.digikey.com/PDF/Data%20Sheets/Sullins%20PDFs/xRxCzzzSxxN-RC_ST_11635-B.pdf" H 6550 1550 50  0001 C CNN
F 4 "Sullins" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "PRPC008SAAN-RC" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "S1011EC-08-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "y" H 0   0   50  0001 C CNN "Src Any/Spec"
F 8 "Connector Header Through Hole 8 position 0.100\" (2.54mm)" H 0   0   50  0001 C CNN "Description"
F 9 "Shenzhen Cankemeng" H 0   0   50  0001 C CNN "LCSC_Mfr"
F 10 "C124381" H 0   0   50  0001 C CNN "LCSC_PN"
	1    6550 1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 1950 6350 1850
Wire Wire Line
	6350 1850 6350 1750
Connection ~ 6350 1850
Wire Wire Line
	6350 1750 6350 1650
Connection ~ 6350 1750
Wire Wire Line
	6350 1650 6350 1550
Connection ~ 6350 1650
Wire Wire Line
	6350 1550 6350 1450
Connection ~ 6350 1550
Wire Wire Line
	6350 1450 6350 1350
Connection ~ 6350 1450
Wire Wire Line
	6350 1350 6350 1250
Connection ~ 6350 1350
$Comp
L Connector_Generic:Conn_01x08 J3
U 1 1 5C861CDA
P 8700 1550
F 0 "J3" H 8650 1950 50  0000 L CNN
F 1 "Conn_01x08" H 8780 1451 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x08_P2.54mm_Vertical" H 8700 1550 50  0001 C CNN
F 3 "https://media.digikey.com/PDF/Data%20Sheets/Sullins%20PDFs/xRxCzzzSxxN-RC_ST_11635-B.pdf" H 8700 1550 50  0001 C CNN
F 4 "Sullins" H 300 0   50  0001 C CNN "DK_Mfr"
F 5 "PRPC008SAAN-RC" H 300 0   50  0001 C CNN "DK_Mfr_PN"
F 6 "S1011EC-08-ND" H 300 0   50  0001 C CNN "DK_PN"
F 7 "y" H 300 0   50  0001 C CNN "Src Any/Spec"
F 8 "Connector Header Through Hole 8 position 0.100\" (2.54mm)" H 300 0   50  0001 C CNN "Description"
F 9 "Shenzhen Cankemeng" H 300 0   50  0001 C CNN "LCSC_Mfr"
F 10 "C124381" H 300 0   50  0001 C CNN "LCSC_PN"
	1    8700 1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	8500 1950 8500 1850
Wire Wire Line
	8500 1850 8500 1750
Connection ~ 8500 1850
Wire Wire Line
	8500 1750 8500 1650
Connection ~ 8500 1750
Wire Wire Line
	8500 1650 8500 1550
Connection ~ 8500 1650
Wire Wire Line
	8500 1550 8500 1450
Connection ~ 8500 1550
Wire Wire Line
	8500 1450 8500 1350
Connection ~ 8500 1450
Wire Wire Line
	8500 1350 8500 1250
Connection ~ 8500 1350
$Comp
L Connector_Generic:Conn_01x08 J4
U 1 1 5C865D89
P 8150 1550
F 0 "J4" H 8150 1950 50  0000 L CNN
F 1 "Conn_01x08" H 8230 1451 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x08_P2.54mm_Vertical" H 8150 1550 50  0001 C CNN
F 3 "https://media.digikey.com/PDF/Data%20Sheets/Sullins%20PDFs/xRxCzzzSxxN-RC_ST_11635-B.pdf" H 8150 1550 50  0001 C CNN
F 4 "Sullins" H 300 0   50  0001 C CNN "DK_Mfr"
F 5 "PRPC008SAAN-RC" H 300 0   50  0001 C CNN "DK_Mfr_PN"
F 6 "S1011EC-08-ND" H 300 0   50  0001 C CNN "DK_PN"
F 7 "y" H 300 0   50  0001 C CNN "Src Any/Spec"
F 8 "Connector Header Through Hole 8 position 0.100\" (2.54mm)" H 300 0   50  0001 C CNN "Description"
F 9 "Shenzhen Cankemeng" H 300 0   50  0001 C CNN "LCSC_Mfr"
F 10 "C124381" H 300 0   50  0001 C CNN "LCSC_PN"
	1    8150 1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	7950 1950 7950 1850
Wire Wire Line
	7950 1850 7950 1750
Connection ~ 7950 1850
Wire Wire Line
	7950 1750 7950 1650
Connection ~ 7950 1750
Wire Wire Line
	7950 1650 7950 1550
Connection ~ 7950 1650
Wire Wire Line
	7950 1550 7950 1450
Connection ~ 7950 1550
Wire Wire Line
	7950 1450 7950 1350
Connection ~ 7950 1450
Wire Wire Line
	7950 1350 7950 1250
Connection ~ 7950 1350
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J1
U 1 1 5C890277
P 2300 2200
F 0 "J1" H 2350 1900 50  0000 C CNN
F 1 "AUX2" H 2350 2526 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x05_P2.54mm_Vertical" H 2300 2200 50  0001 C CNN
F 3 "https://drawings-pdf.s3.amazonaws.com/10492.pdf" H 2300 2200 50  0001 C CNN
F 4 "Sullins" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "PPTC052LFBN-RC" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "S6105-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "y" H 0   0   50  0001 C CNN "Src Any/Spec"
F 8 "10 Position Header Connector 0.100\" (2.54mm) Through Hole Tin" H 0   0   50  0001 C CNN "Description"
F 9 "Boom Precision Elec" H 0   0   50  0001 C CNN "LCSC_Mfr"
F 10 "C30419" H 0   0   50  0001 C CNN "LCSC_PN"
	1    2300 2200
	-1   0    0    1   
$EndComp
Text GLabel 2500 2400 2    50   UnSpc ~ 0
+V_LOGIC
Text GLabel 2500 2300 2    50   UnSpc ~ 0
A4
Text GLabel 2500 2200 2    50   UnSpc ~ 0
A6
Text GLabel 2500 2100 2    50   UnSpc ~ 0
AD14
Text GLabel 2500 2000 2    50   UnSpc ~ 0
UART3_TX_LV
$Comp
L LCD-panel-adapter-lvc-rescue:GND #PWR05
U 1 1 5C8AEB9C
P 1950 2450
F 0 "#PWR05" H 1950 2450 30  0001 C CNN
F 1 "GND" H 1950 2380 30  0001 C CNN
F 2 "" H 1950 2450 60  0000 C CNN
F 3 "" H 1950 2450 60  0000 C CNN
	1    1950 2450
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1950 2450 1950 2400
Wire Wire Line
	1950 2400 2000 2400
Text GLabel 2000 2300 0    50   UnSpc ~ 0
A5
Text GLabel 2000 2200 0    50   UnSpc ~ 0
A7
Text GLabel 2000 2100 0    50   UnSpc ~ 0
AD15
Text GLabel 2000 2000 0    50   UnSpc ~ 0
UART3_RX_LV
Text GLabel 6350 1250 0    50   Input ~ 0
SD_DO
Text GLabel 8500 1250 0    50   Output ~ 0
SD_DI
Text GLabel 7950 1250 0    50   Input ~ 0
SD_CLK
Text GLabel 7150 1250 0    50   UnSpc ~ 0
AD14
Text GLabel 7150 1350 0    50   UnSpc ~ 0
A6
Text GLabel 7150 1450 0    50   UnSpc ~ 0
A4
Text GLabel 7150 1550 0    50   UnSpc ~ 0
AD15
Text GLabel 7150 1650 0    50   UnSpc ~ 0
A7
Text GLabel 7150 1750 0    50   UnSpc ~ 0
A5
Text Notes 7400 1300 0    50   ~ 0
X_CS
Text Notes 7400 1400 0    50   ~ 0
Y_CS
Text Notes 7400 1500 0    50   ~ 0
Z_CS
Text Notes 7400 1600 0    50   ~ 0
E0_CS
Text Notes 7400 1700 0    50   ~ 0
E1_CS
Text Notes 7400 1800 0    50   ~ 0
E2_CS
Text Notes 6100 1050 0    100  ~ 0
SPI Breakout (for TMC2130, etc.)
$Comp
L Connector_Generic:Conn_01x08 J5
U 1 1 5C832222
P 7350 1550
F 0 "J5" H 7300 1950 50  0000 L CNN
F 1 "Conn_01x08" H 7430 1451 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x08_P2.54mm_Vertical" H 7350 1550 50  0001 C CNN
F 3 "https://media.digikey.com/PDF/Data%20Sheets/Sullins%20PDFs/xRxCzzzSxxN-RC_ST_11635-B.pdf" H 7350 1550 50  0001 C CNN
F 4 "Sullins" H 800 0   50  0001 C CNN "DK_Mfr"
F 5 "PRPC008SAAN-RC" H 800 0   50  0001 C CNN "DK_Mfr_PN"
F 6 "S1011EC-08-ND" H 800 0   50  0001 C CNN "DK_PN"
F 7 "y" H 800 0   50  0001 C CNN "Src Any/Spec"
F 8 "Connector Header Through Hole 8 position 0.100\" (2.54mm)" H 800 0   50  0001 C CNN "Description"
F 9 "Shenzhen Cankemeng" H 800 0   50  0001 C CNN "LCSC_Mfr"
F 10 "C124381" H 800 0   50  0001 C CNN "LCSC_PN"
	1    7350 1550
	1    0    0    -1  
$EndComp
Text GLabel 7150 1950 0    50   UnSpc ~ 0
UART3_RX_LV
Text GLabel 7150 1850 0    50   UnSpc ~ 0
UART3_TX_LV
$EndSCHEMATC
